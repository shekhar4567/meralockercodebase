package com.meralocker.app;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.meralocker.dataconnectivity.DataConnectivity;
import com.meralocker.http.HttpParam;
import com.meralocker.http.Url;
import com.meralocker.toastmessage.ToastMessage;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class CheckMobileActivity extends ActionBarActivity {

	Context ctx;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_verify_mobile);

		customiseBar();
		Button okBtn = (Button) findViewById(R.id.ok);
		Button cancelBtn = (Button) findViewById(R.id.cancel);
		// ((EditText) CheckMobileActivity.this.findViewById(R.id.mobile))
		// .setText("9890966143");

		ctx = CheckMobileActivity.this;

		okBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (DataConnectivity.isConnected(ctx)) {
					String number = ((EditText) CheckMobileActivity.this
							.findViewById(R.id.mobile)).getText().toString();
					if (number.length() == 10) {
						new verifyMobileTask().execute();
					} else {

						ToastMessage.toastMessage(ctx,
								ctx.getString(R.string.valid_mobile));

					}
				} else {
					ToastMessage.toastMessage(ctx,
							ctx.getString(R.string.no_internet));
				}
			}
		});

		cancelBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private class verifyMobileTask extends AsyncTask<Void, Void, Void> {

		String getresponse;
		HttpPost httppost;

		ProgressDialog dialog;
		final List<BasicNameValuePair> postParameters = new ArrayList<BasicNameValuePair>();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new ProgressDialog(ctx);
			dialog.setMessage("Verifying..!!");
			dialog.show();

		}

		@Override
		protected Void doInBackground(Void... params) {

			try {

				// Add your data

				HttpParam.httpClientObject = new DefaultHttpClient();

				httppost = new HttpPost(
						Url.domainUrl
								+ Url.verifyMobileUrl
								+ ((EditText) CheckMobileActivity.this
										.findViewById(R.id.mobile)).getText()
										.toString() + "/user");

				// Execute HTTP Post Request
				HttpResponse response = HttpParam.httpClientObject
						.execute(httppost);
				getresponse = EntityUtils.toString(response.getEntity());
				HttpParam.httpObject = httppost;
				System.out.println(HttpParam.httpClientObject.getParams());
				// Log.e("getresponse", obj);
				Log.e("getresponse", getresponse);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			try {
				JSONObject responseObject = new JSONObject(getresponse);
				JSONObject dataObj = null;

				String statusCode = responseObject.optString("status");

				if (statusCode.equalsIgnoreCase("success")) {
					dataObj = responseObject.getJSONObject("data");
					String userid = dataObj.optString("id");
					alertRegisteredUser(userid);
				} else {
					alertUser();
				}

			} catch (Exception e) {
				ToastMessage.toastMessage(ctx,
						ctx.getString(R.string.exception));
				e.printStackTrace();
			} finally {
				if (dialog.isShowing()) {
					dialog.dismiss();
				}

			}
			if (dialog.isShowing()) {
				dialog.dismiss();
			}

		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			// Toast.makeText(getApplication(), "Back",
			// Toast.LENGTH_LONG).show();
			return true;
		}
		if (id == android.R.id.home) {

			ToastMessage.toastMessage(ctx, ctx.getString(R.string.back));
			finish();
			return true;

		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

	private void alertUser() {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setMessage(ctx.getString(R.string.register))
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// do things
						Intent i = new Intent(ctx,
								UserResgistrationActivity.class);
						navigate(i);
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void alertRegisteredUser(final String userId) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setMessage(ctx.getString(R.string.otp)).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// do things
						Intent i = new Intent(ctx,
								UserSmsVerificationActivity.class);
						i.putExtra("UserId", userId);

						navigate(i);
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void navigate(Intent i) {

		i.putExtra("UserType", getIntent().getStringExtra("UserType"));
		i.putExtra("MobileNo", ((EditText) CheckMobileActivity.this
				.findViewById(R.id.mobile)).getText().toString());
		startActivityForResult(i, 500);
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	private void customiseBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#0066C6")));
	}

}
