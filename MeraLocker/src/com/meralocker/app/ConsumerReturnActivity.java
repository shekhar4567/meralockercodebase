package com.meralocker.app;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

public class ConsumerReturnActivity extends ActionBarActivity {
	EditText codeTxt;
	RadioButton radioReturnCode, radioMLCOde;
	LinearLayout returnCodeLayout1, returnCodeLayout2, meralockercode,
			meralockerCodeLayout;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_consumer_return);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#0066C6")));
		Button okBtn = (Button) findViewById(R.id.ok);
		Button cancelBtn = (Button) findViewById(R.id.cancel);
		returnCodeLayout1 = (LinearLayout) findViewById(R.id.returncodelayout1);
		returnCodeLayout2 = (LinearLayout) findViewById(R.id.returncodelayout2);
		meralockerCodeLayout = (LinearLayout) findViewById(R.id.meralockercode);

		RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radio);
		radioReturnCode = (RadioButton) findViewById(R.id.return_code);
		radioMLCOde = (RadioButton) findViewById(R.id.meralocker_code);
		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// checkedId is the RadioButton selected
				if (radioReturnCode.isChecked()) {
					returnCodeLayout1.setVisibility(View.VISIBLE);
					returnCodeLayout2.setVisibility(View.VISIBLE);
					meralockerCodeLayout.setVisibility(View.GONE);

				} else {
					returnCodeLayout1.setVisibility(View.GONE);
					returnCodeLayout2.setVisibility(View.GONE);
					meralockerCodeLayout.setVisibility(View.VISIBLE);
				}
			}
		});

		okBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				String phoneNo = ((EditText) ConsumerReturnActivity.this
						.findViewById(R.id.phone)).getText().toString();
				String verifcode = ((EditText) ConsumerReturnActivity.this
						.findViewById(R.id.vcode)).getText().toString();
				String lname = ((EditText) ConsumerReturnActivity.this
						.findViewById(R.id.lname)).getText().toString();
				String rcode = ((EditText) ConsumerReturnActivity.this
						.findViewById(R.id.rcode)).getText().toString();
				String rercode = ((EditText) ConsumerReturnActivity.this
						.findViewById(R.id.rercode)).getText().toString();
				String meralockercode = ((EditText) ConsumerReturnActivity.this
						.findViewById(R.id.lockercode)).getText().toString();

				if (phoneNo.length() > 0 && lname.length() > 0
						&& verifcode.length() > 0) {

					Intent i = new Intent(ConsumerReturnActivity.this,
							LockerOpenedActivity.class);
					i.putExtra("User", "Consumer");
					navigate(i);
				} else {
					Toast.makeText(getApplication(), "Enter all the fields",
							Toast.LENGTH_LONG).show();
				}

				// String phoneNo =
				// if (phoneTxt.getText().toString().length() > 0
				// && rePhoneTxt.getText().toString().length() > 0
				// && lNameTxt.getText().toString().length() > 0
				// && fNameTxt.getText().toString().length() > 0) {
				//
				// Intent intent = new Intent(getApplicationContext(),
				// MainActivity.class);
				// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				// intent.putExtra("EXIT", true);
				// startActivity(intent);
				// }

			}
		});
		cancelBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			// Toast.makeText(getApplication(), "Back",
			// Toast.LENGTH_LONG).show();
			return true;
		}
		if (id == android.R.id.home) {
			Toast.makeText(getApplication(), "Back", Toast.LENGTH_LONG).show();
			finish();
			return true;

		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

	private void navigate(Intent i) {

		startActivityForResult(i, 500);
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

}
