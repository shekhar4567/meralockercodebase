package com.meralocker.app;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.meralocker.http.HttpParam;
import com.meralocker.http.Url;
import com.meralocker.toastmessage.ToastMessage;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class PickLockerSizeActivity extends ActionBarActivity {

	String flagSize;
	Context ctx;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pick_size);

		customiseBar();
		Button okBtn = (Button) findViewById(R.id.ok);
		Button cancelBtn = (Button) findViewById(R.id.cancel);

		final Button smallBtn = (Button) findViewById(R.id.small);
		final Button medBtn = (Button) findViewById(R.id.medium);
		final Button largeBtn = (Button) findViewById(R.id.large);

		ctx = PickLockerSizeActivity.this;

		okBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				new deliverPackageTask().execute();

			}
		});

		cancelBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		smallBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				flagSize = "SMALL";
				smallBtn.setBackgroundColor(Color.parseColor("#0066C6"));
				medBtn.setBackgroundResource(android.R.drawable.btn_default);
				largeBtn.setBackgroundResource(android.R.drawable.btn_default);
			}
		});

		medBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				flagSize = "MEDIUM";
				medBtn.setBackgroundColor(Color.parseColor("#0066C6"));
				smallBtn.setBackgroundResource(android.R.drawable.btn_default);
				largeBtn.setBackgroundResource(android.R.drawable.btn_default);
			}
		});

		largeBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				flagSize = "LARGE";
				largeBtn.setBackgroundColor(Color.parseColor("#0066C6"));
				smallBtn.setBackgroundResource(android.R.drawable.btn_default);
				medBtn.setBackgroundResource(android.R.drawable.btn_default);
			}
		});

	}

	private class deliverPackageTask extends AsyncTask<Void, Void, Void> {

		String getresponse;

		ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new ProgressDialog(PickLockerSizeActivity.this);
			dialog.setMessage("Processing your request..!!");
			dialog.show();

		}

		// doeps5472f
		@Override
		protected Void doInBackground(Void... params) {

			try {

				HttpPost httpobj = new HttpPost(Url.domainUrl
						+ Url.deliveryPackageUrl + "1/locker/" + flagSize
						+ "/deliver");

				JSONObject obj = new JSONObject();
				JSONObject obj1 = new JSONObject();
				obj1.put("mobileNumber",
						getIntent().getStringExtra("Delivery_Mobile_Number"));
				obj.put("receiver", obj1);
				obj.put("delivererId", getIntent().getStringExtra("UserId"));

				StringEntity se1 = new StringEntity(obj.toString());
				httpobj.setEntity(se1);
				// Execute HTTP Post Request
				HttpResponse response = HttpParam.httpClientObject
						.execute(httpobj);
				getresponse = EntityUtils.toString(response.getEntity());

				// Logs
				Log.e("UserId",
						getIntent().getStringExtra("Delivery_Mobile_Number"));
				Log.e("HTTpurl", Url.domainUrl + Url.deliveryPackageUrl
						+ "1/locker/" + flagSize + "/deliver");
				Log.e("Post_json", obj.toString());
				// Log.e("getresponse", obj);
				Log.e("Http_response", getresponse);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			try {
				JSONObject responseObject = new JSONObject(getresponse);

				String statusCode = responseObject.optString("status");
				String message = responseObject.optString("message");

				if (statusCode.equalsIgnoreCase("success")) {

					Intent i = new Intent(ctx, LockerOpenedActivity.class);
					navigate(i);

				} else {

					ToastMessage.toastMessage(ctx, message);
					Intent intent = new Intent(ctx, MainActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("EXIT", true);
					startActivity(intent);

				}

			} catch (Exception e) {
				ToastMessage.toastMessage(ctx,
						ctx.getString(R.string.exception));
				e.printStackTrace();
			} finally {
				if (dialog.isShowing()) {
					dialog.dismiss();
				}
				
			}
			if (dialog.isShowing()) {
				dialog.dismiss();
			}

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			// Toast.makeText(getApplication(), "Back",
			// Toast.LENGTH_LONG).show();
			return true;
		}
		if (id == android.R.id.home) {
			ToastMessage.toastMessage(ctx, ctx.getString(R.string.back));
			finish();
			return true;

		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

	private void navigate(Intent i) {
		// i.putExtra("UserId", getIntent().getStringExtra("UserId"));
		startActivityForResult(i, 500);
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	private void customiseBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#0066C6")));
	}

}
