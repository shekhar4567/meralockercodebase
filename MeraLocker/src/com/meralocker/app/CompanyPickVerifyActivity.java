package com.meralocker.app;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class CompanyPickVerifyActivity extends ActionBarActivity {
	EditText codeTxt;
	RadioButton radioReturnCode, radioMLCOde;
	LinearLayout returnCodeLayout1, returnCodeLayout2, meralockercode,
			meralockerCodeLayout,Layout2;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_consumer_return);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#0066C6")));
		Button okBtn = (Button) findViewById(R.id.ok);
		Button cancelBtn = (Button) findViewById(R.id.cancel);
		returnCodeLayout1 = (LinearLayout) findViewById(R.id.returncodelayout1);
		Layout2 = (LinearLayout) findViewById(R.id.layout2);
		returnCodeLayout2 = (LinearLayout) findViewById(R.id.returncodelayout2);
		meralockerCodeLayout = (LinearLayout) findViewById(R.id.meralockercode);

		RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radio);
		radioGroup.setVisibility(View.GONE);
		Layout2.setVisibility(View.GONE);
		meralockerCodeLayout.setVisibility(View.VISIBLE);
		((TextView) this.findViewById(R.id.label)).setText(R.string.pick);

		okBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String phoneNo = ((EditText) CompanyPickVerifyActivity.this.findViewById(R.id.phone)).getText().toString();
				String lname = ((EditText) CompanyPickVerifyActivity.this.findViewById(R.id.lname)).getText().toString();
				String meralockercode = ((EditText) CompanyPickVerifyActivity.this.findViewById(R.id.lockercode)).getText().toString();
				String rcode = ((EditText) CompanyPickVerifyActivity.this.findViewById(R.id.rcode)).getText().toString();
				String rercode = ((EditText) CompanyPickVerifyActivity.this.findViewById(R.id.rercode)).getText().toString();
				if (phoneNo.length() > 0
						&& lname.length() > 0
						&& meralockercode.length() > 0) {

					Intent i = new Intent(CompanyPickVerifyActivity.this,
							LockerOpenedActivity.class);
					i.putExtra("User", "Consumer");
					navigate(i);
				}else{
					Toast.makeText(getApplication(), "Enter all the fields",
							Toast.LENGTH_LONG).show();
				}

			}
		});
		cancelBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			// Toast.makeText(getApplication(), "Back",
			// Toast.LENGTH_LONG).show();
			return true;
		}
		if (id == android.R.id.home) {
			Toast.makeText(getApplication(), "Back", Toast.LENGTH_LONG).show();
			finish();
			return true;

		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

	private void navigate(Intent i) {

		startActivityForResult(i, 500);
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

}
