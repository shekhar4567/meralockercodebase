package com.meralocker.app;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.meralocker.dataconnectivity.DataConnectivity;
import com.meralocker.http.HttpParam;
import com.meralocker.http.Url;
import com.meralocker.toastmessage.ToastMessage;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UserSmsVerificationActivity extends ActionBarActivity {
	EditText codeTxt;
	String otpCode;
	Context ctx;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sms_verification);

		customiseBar();

		Button okBtn = (Button) findViewById(R.id.ok);
		Button cancelBtn = (Button) findViewById(R.id.cancel);

		final EditText code1Txt = (EditText) findViewById(R.id.verifycode1);
		final EditText code2Txt = (EditText) findViewById(R.id.verifycode2);
		final EditText code3Txt = (EditText) findViewById(R.id.verifycode3);
		final EditText code4Txt = (EditText) findViewById(R.id.verifycode4);
		final EditText code5Txt = (EditText) findViewById(R.id.verifycode5);
		final EditText code6Txt = (EditText) findViewById(R.id.verifycode6);

		ctx = UserSmsVerificationActivity.this;

		okBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (DataConnectivity.isConnected(ctx)) {
					String code1 = ((EditText) UserSmsVerificationActivity.this
							.findViewById(R.id.verifycode1)).getText()
							.toString();
					String code2 = ((EditText) UserSmsVerificationActivity.this
							.findViewById(R.id.verifycode2)).getText()
							.toString();
					String code3 = ((EditText) UserSmsVerificationActivity.this
							.findViewById(R.id.verifycode3)).getText()
							.toString();
					String code4 = ((EditText) UserSmsVerificationActivity.this
							.findViewById(R.id.verifycode4)).getText()
							.toString();
					String code5 = ((EditText) UserSmsVerificationActivity.this
							.findViewById(R.id.verifycode5)).getText()
							.toString();
					String code6 = ((EditText) UserSmsVerificationActivity.this
							.findViewById(R.id.verifycode6)).getText()
							.toString();
					otpCode = code1 + code2 + code3 + code4 + code5 + code6;
					if (otpCode.toString().length() == 6) {
						new verifyCodeTask().execute();
					} else {

						ToastMessage.toastMessage(ctx,
								ctx.getString(R.string.valid_code));
					}
				} else {
					ToastMessage.toastMessage(ctx,
							ctx.getString(R.string.no_internet));
				}

			}
		});

		cancelBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		code1Txt.addTextChangedListener(new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				Integer textlength = code1Txt.getText().length();

				if (textlength == 1) {
					code2Txt.requestFocus();

				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}
		});

		code2Txt.addTextChangedListener(new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				Integer textlength = code2Txt.getText().length();

				if (textlength == 1) {
					code3Txt.requestFocus();

				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}
		});

		code3Txt.addTextChangedListener(new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				Integer textlength = code3Txt.getText().length();

				if (textlength == 1) {
					code4Txt.requestFocus();

				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}
		});

		code4Txt.addTextChangedListener(new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				Integer textlength = code4Txt.getText().length();

				if (textlength == 1) {
					code5Txt.requestFocus();

				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}
		});

		code5Txt.addTextChangedListener(new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				Integer textlength = code5Txt.getText().length();

				if (textlength == 1) {
					code6Txt.requestFocus();

				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}
		});

		code6Txt.addTextChangedListener(new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			// Toast.makeText(getApplication(), "Back",
			// Toast.LENGTH_LONG).show();
			return true;
		}
		if (id == android.R.id.home) {

			ToastMessage.toastMessage(ctx, ctx.getString(R.string.back));
			finish();

			return true;

		}
		return super.onOptionsItemSelected(item);
	}

	private class verifyCodeTask extends AsyncTask<Void, Void, Void> {

		String getresponse;
		HttpPost httppost;
		ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new ProgressDialog(ctx);
			dialog.setMessage("Verifying..!!");
			dialog.show();

		}

		@Override
		protected Void doInBackground(Void... params) {

			try {

				// Add your data

				httppost = new HttpPost(Url.domainUrl + Url.verifyOtpUrl);
				JSONObject obj = new JSONObject();
				obj.put("otp", otpCode);
				Log.e("id3", getIntent().getStringExtra("UserId"));
				obj.put("userId", getIntent().getStringExtra("UserId"));
				StringEntity se1 = new StringEntity(obj.toString());
				httppost.setEntity(se1);
				Log.e("json", obj.toString());
				// Execute HTTP Post Request
				HttpResponse response = HttpParam.httpClientObject
						.execute(httppost);
				getresponse = EntityUtils.toString(response.getEntity());
				System.out.println(HttpParam.httpClientObject.getParams());
				// Log.e("getresponse", obj);
				Log.e("getresponse", getresponse);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			try {
				JSONObject responseObject = new JSONObject(getresponse);
				String statusCode = responseObject.optString("status");
				String message = responseObject.optString("message");

				if (statusCode.equalsIgnoreCase("success")) {
					Toast.makeText(getApplication(), "Verified..!",
							Toast.LENGTH_LONG).show();

					if (getIntent().getStringExtra("UserType")
							.equalsIgnoreCase("ROLE_DELIVERER")) {
						Intent i = new Intent(ctx, CompanyHomeActivity.class);

						// Log.e("id2", userid);
						navigate(i);

					} else {
						Intent i = new Intent(ctx, ConsumerHomeActivity.class);

						// Log.e("id2", userid);
						navigate(i);
					}

					// Intent intent = new Intent(getApplicationContext(),
					// MainActivity.class);
					// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					// intent.putExtra("EXIT", true);
					// startActivity(intent);

				} else {
					alertUser(message);
				}

			} catch (Exception e) {
				ToastMessage.toastMessage(ctx,
						ctx.getString(R.string.exception));
				e.printStackTrace();
			} finally {
				if (dialog.isShowing()) {
					dialog.dismiss();
				}

			}
			if (dialog.isShowing()) {
				dialog.dismiss();
			}

		}
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

	private void alertUser(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// do things
						((EditText) UserSmsVerificationActivity.this
								.findViewById(R.id.verifycode1)).setText("");
						((EditText) UserSmsVerificationActivity.this
								.findViewById(R.id.verifycode2)).setText("");
						((EditText) UserSmsVerificationActivity.this
								.findViewById(R.id.verifycode3)).setText("");
						((EditText) UserSmsVerificationActivity.this
								.findViewById(R.id.verifycode4)).setText("");
						((EditText) UserSmsVerificationActivity.this
								.findViewById(R.id.verifycode5)).setText("");
						((EditText) UserSmsVerificationActivity.this
								.findViewById(R.id.verifycode6)).setText("");

						dialog.dismiss();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void navigate(Intent i) {
		i.putExtra("UserId", getIntent().getStringExtra("UserId"));
		i.putExtra("UserType", getIntent().getStringExtra("UserType"));
		startActivityForResult(i, 500);
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	private void customiseBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#0066C6")));
	}
}
