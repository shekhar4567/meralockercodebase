package com.meralocker.app;

import com.meralocker.toastmessage.ToastMessage;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DeliveryDetailActivity extends ActionBarActivity {
	EditText codeTxt;
	Context ctx;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_information);
		customiseBar();
		Button okBtn = (Button) findViewById(R.id.ok);
		Button cancelBtn = (Button) findViewById(R.id.cancel);
		ctx = DeliveryDetailActivity.this;

		okBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String number = ((EditText) DeliveryDetailActivity.this
						.findViewById(R.id.phone)).getText().toString();
				String renumber = ((EditText) DeliveryDetailActivity.this
						.findViewById(R.id.rephone)).getText().toString();
				if (number.length() == 10 && renumber.length() == 10) {
					if (number.equalsIgnoreCase(renumber)) {
						Intent i = new Intent(DeliveryDetailActivity.this,
								PickLockerSizeActivity.class);
						i.putExtra("Delivery_Mobile_Number", number);
						navigate(i);
					} else {

						ToastMessage.toastMessage(ctx,
								ctx.getString(R.string.mismatch));
					}
				} else {
					ToastMessage.toastMessage(ctx,
							ctx.getString(R.string.valid_mobile));

				}

			}
		});

		cancelBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			// Toast.makeText(getApplication(), "Back",
			// Toast.LENGTH_LONG).show();
			return true;
		}
		if (id == android.R.id.home) {
			Toast.makeText(getApplication(), "Back", Toast.LENGTH_LONG).show();
			finish();
			return true;

		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

	private void navigate(Intent i) {
		i.putExtra("UserId", getIntent().getStringExtra("UserId"));
		startActivityForResult(i, 500);
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	private void customiseBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#0066C6")));
	}

}
