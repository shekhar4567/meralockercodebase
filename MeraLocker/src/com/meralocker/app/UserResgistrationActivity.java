package com.meralocker.app;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.meralocker.dataconnectivity.DataConnectivity;
import com.meralocker.http.Url;
import com.meralocker.toastmessage.ToastMessage;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class UserResgistrationActivity extends ActionBarActivity {
	EditText phoneTxt, fNameTxt, lNameTxt, emailTxt;
	AutoCompleteTextView deliveryCompany;
	Context ctx;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_registeration);

		customiseBar();
		Button okBtn = (Button) findViewById(R.id.ok);
		Button cancelBtn = (Button) findViewById(R.id.cancel);
		phoneTxt = (EditText) findViewById(R.id.phone);
		lNameTxt = (EditText) findViewById(R.id.lname);
		fNameTxt = (EditText) findViewById(R.id.fname);
		emailTxt = (EditText) findViewById(R.id.email);
		LinearLayout companyLayout = (LinearLayout) findViewById(R.id.layoutcompany);
		phoneTxt.setText(getIntent().getStringExtra("MobileNo"));
		phoneTxt.setKeyListener(null);

		ctx = UserResgistrationActivity.this;

		deliveryCompany = (AutoCompleteTextView) findViewById(R.id.deliverycompany);
		String[] companies = { "Amazon", "Flipkart", "Myntra", "jabong",
				"yupme", "firstcry", "Ask Me Bazar", "Paytm" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.select_dialog_item, companies);
		// Getting the instance of AutoCompleteTextView

		if (getIntent().getStringExtra("UserType").equalsIgnoreCase(
				"ROLE_CONSUMER")) {
			companyLayout.setVisibility(View.GONE);
		}

		deliveryCompany.setThreshold(1);// will start working from first
										// character
		deliveryCompany.setAdapter(adapter);// setting the adapter data into the

		okBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});

		okBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (DataConnectivity.isConnected(ctx)) {
					if (getIntent().getStringExtra("UserType")
							.equalsIgnoreCase("ROLE_CONSUMER")) {
						if (phoneTxt.getText().toString().length() > 0
								&& emailTxt.getText().toString().length() > 0
								&& lNameTxt.getText().toString().length() > 0
								&& fNameTxt.getText().toString().length() > 0) {

							if (phoneTxt.getText().toString().length() == 10) {
								if (isValidEmail(emailTxt.getText().toString())) {

									new registerationTask().execute();

								} else {

									ToastMessage.toastMessage(ctx,
											ctx.getString(R.string.valid_email));
								}
							} else {

								ToastMessage.toastMessage(ctx,
										ctx.getString(R.string.valid_mobile));
							}
						} else {
							ToastMessage.toastMessage(ctx,
									ctx.getString(R.string.all_fieldsl));

						}
					} else {
						if (phoneTxt.getText().toString().length() > 0
								&& emailTxt.getText().toString().length() > 0
								&& lNameTxt.getText().toString().length() > 0
								&& fNameTxt.getText().toString().length() > 0
								&& deliveryCompany.getText().toString()
										.length() > 0) {

							if (phoneTxt.getText().toString().length() == 10) {
								if (isValidEmail(emailTxt.getText().toString())) {

									new registerationTask().execute();

								} else {

									ToastMessage.toastMessage(ctx,
											ctx.getString(R.string.valid_email));
								}
							} else {

								ToastMessage.toastMessage(ctx,
										ctx.getString(R.string.valid_mobile));
							}
						} else {

							ToastMessage.toastMessage(ctx,
									ctx.getString(R.string.all_fieldsl));

						}
					}
				} else {
					ToastMessage.toastMessage(ctx,
							ctx.getString(R.string.no_internet));
				}

			}
		});
		cancelBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}

	private class registerationTask extends AsyncTask<Void, Void, Void> {

		String getresponse;
		HttpPost httppost;

		ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new ProgressDialog(ctx);
			dialog.setMessage("Creating your profile..!!");
			dialog.show();

		}

		@Override
		protected Void doInBackground(Void... params) {

			try {
				HttpClient httpclient;

				// Add your data

				httpclient = new DefaultHttpClient();
				httppost = new HttpPost(Url.domainUrl + Url.registerUrl);
				JSONObject obj = new JSONObject();
				obj.put("firstName", fNameTxt.getText().toString());
				obj.put("lastName", lNameTxt.getText().toString());
				obj.put("role", getIntent().getStringExtra("UserType"));
				obj.put("email", emailTxt.getText().toString());
				obj.put("mobileNumber", phoneTxt.getText().toString());
				obj.put("company", deliveryCompany.getText().toString());
				StringEntity se1 = new StringEntity(obj.toString());
				httppost.setEntity(se1);
				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				getresponse = EntityUtils.toString(response.getEntity());

				Log.e("role", getIntent().getStringExtra("UserType"));
				Log.e("Post_json", obj.toString());
				// Log.e("getresponse", obj);
				Log.e("Http_response", getresponse);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			try {
				JSONObject responseObject = new JSONObject(getresponse);
				JSONObject dataObj = null;

				String statusCode = responseObject.optString("status");
				String message = responseObject.optString("message");

				if (statusCode.equalsIgnoreCase("success")) {
					dataObj = responseObject.getJSONObject("data");
					String userid = dataObj.optString("id");
					Log.e("id1", userid);
					alertUser(ctx.getString(R.string.profile_created), userid);

				} else {

				}

			} catch (Exception e) {
				ToastMessage.toastMessage(ctx,
						ctx.getString(R.string.exception));
				e.printStackTrace();
			} finally {
				if (dialog.isShowing()) {
					dialog.dismiss();
				}

			}
			if (dialog.isShowing()) {
				dialog.dismiss();
			}

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			// Toast.makeText(getApplication(), "Back",
			// Toast.LENGTH_LONG).show();
			return true;
		}
		if (id == android.R.id.home) {
			ToastMessage.toastMessage(ctx, ctx.getString(R.string.back));
			finish();
			return true;

		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

	public final static boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		}
	}

	private void alertUser(String message, final String userid) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// do things
						dialog.dismiss();
						Intent i = new Intent(ctx,
								UserSmsVerificationActivity.class);
						i.putExtra("UserId", userid);
						Log.e("id2", userid);
						navigate(i);

					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void navigate(Intent i) {

		i.putExtra("UserType", getIntent().getStringExtra("UserType"));
		startActivityForResult(i, 500);
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	private void customiseBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#0066C6")));
	}

}
